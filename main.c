#include <SOIL/SOIL.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <string.h>
#include <math.h>
#include "nave.h"
#include "utilidades.h"
#include "asteroids.h"

// Para ganhar pontinhos extras
typedef enum tecla{
  UP = 0,
  DOWN,
  LEFT,
  RIGHT,
  SPACE,
  W,
  A,
  D,
  C,
  P,
  R
} TECLA;

typedef enum textura{
  NAVE1 = 0,
  NAVE2,
  TIROTEX,
  ASTEROID1,
  ASTEROID2,
  ASTEROID3,
  ASTEROID4,
  ASTEROID5,
  ASTEROID6,
  MAPA
} TEXTURAS;

typedef struct mouse {
  GLfloat x;
  GLfloat y;
} MOUSE;

MOUSE mouse;

void inicializa();
void desenhaCena();
void desenhaCenaInicial();
void teclado(unsigned char key, int x, int y);
void tecladoUP(unsigned char key, int x, int y);
void teclado_Special(int key, int x, int y);
void teclado_SpecialUP(int key, int x, int y);
void redimensiona(int h, int w);
void atualiza(int idx);
void checaColisaoNaveTiro(NAVE *nave);  // Função para checar colisão
void click(int button, int state, int x, int y);

// Criação de uma variável do tipo NAVE
NAVE nave1;
NAVE nave2;   // Para teste de colisão

// Variável que mudará de estado quando uma tecla for precionada
int tecladoState[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int liberaTiro1 = 1; // Variável para liberar para fazer o próximo disparo
int liberaTiro2 = 1; // Variável para liberar para fazer o próximo disparo

int stage     = 1;  // Estagio que a tela se encontra
int numPlayer = 1;  // Número de jogadores

int main(int argc, char** argv){

  // Acorda o glut
  glutInit(&argc, argv);

  // Define a versão do glut que será utilizada
  glutInitContextVersion(1,1);
  glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

  // Configuração inicial da tela
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA); // Configura como duas telas para quando for atualizar a tela usar a função glutSwapBuffers();
  glutInitWindowSize(500,500);  // 500 pixels x 500 pixels
  glutInitWindowPosition(100, 100); // Define o número limite da tela

  // Abre a janela "Paradoroides"
  glutCreateWindow("{Parad}oroides");

  // Para textura
  init();

  // CALLBACKS para o clique do MOUSE
  glutMouseFunc(click);

  // REGISTRO DE CALLBACKS

  // else if (stage == 2)
  glutDisplayFunc(desenhaCena);

  glutReshapeFunc(redimensiona);

  glutKeyboardFunc(teclado);
  glutKeyboardUpFunc(tecladoUP);

  glutSpecialFunc(teclado_Special);
  glutSpecialUpFunc(teclado_SpecialUP);

  glutTimerFunc(0, atualiza, 0);

  inicializa();

  glutMainLoop();
  return 0;
}

//@TODO: Fazer descrição das funções
void inicializa(){
  glClearColor(0, 0, 0, 0);    // Cor branca
  inicializaVariaveisNave(&nave1, 0.0, 0.0, 0.0); //vel = 0.8
  inicializaVariaveisNave(&nave2, -20.0, -20.0, 0.0);
  inicializaListaAsteroids();
  iniciaListaTiros();

}

// DESENHA CENA
void desenhaCena(){

  if (stage == 1) desenhaCenaInicial();

  else if (stage == 3){
    glClear(GL_COLOR_BUFFER_BIT);

    char textoPlayer1[50] = {"Player1     "};
    char textoPlayer2[50] = {"Player2     "};
    char texto2Player1[20] = {"Vidas   "};
    char texto2Player2[20] = {"Vidas   "};

    nave1.imagem = NAVE1;
    if (numPlayer == 2) nave2.imagem = NAVE2;

    // N A V E  1
    if (tecladoState[RIGHT])      alteraRotacao (&nave1, '-'); // Aperta para direita
    if (tecladoState[LEFT])       alteraRotacao (&nave1, '+'); // Aperta para esquerda
    if (tecladoState[UP])    	    naveAcelera   (&nave1, '+');    // Aperta para cima
    else if (!tecladoState[UP])   naveAcelera   (&nave1, '-');    // Solta para cima
    if (tecladoState[SPACE] && liberaTiro1){
      naveDispara(&nave1);
      liberaTiro1 = 0;
      //nave1.pontuacao ++;
    }
    if (!tecladoState[SPACE])     liberaTiro1 = 1;

    if (numPlayer == 2){
      // N A V E  2
      if (tecladoState[D])        alteraRotacao (&nave2, '-'); // Aperta para direita
      if (tecladoState[A])        alteraRotacao (&nave2, '+'); // Aperta para esquerda
      if (tecladoState[W])    	  naveAcelera   (&nave2, '+');    // Aperta para cima
      else if (!tecladoState[W])  naveAcelera   (&nave2, '-');    // Solta para cima
      if (tecladoState[C] && liberaTiro2){
        naveDispara(&nave2);
        liberaTiro2 = 0;
        //nave2.pontuacao ++;
      }
      if (!tecladoState[C])     liberaTiro2 = 1;
    }

    desenhaFundo();   // muda tela
    transladaNaveComRotacao(&nave1);
    if (numPlayer == 2) transladaNaveComRotacao(&nave2);
    transladaAsteroid();
    transladaTiro();

    // P O N T U A Ç Ã O  D O  J O G O
    sprintf(&textoPlayer1[10], "%d", nave1.pontuacao);
    sprintf(&textoPlayer2[10], "%d", nave2.pontuacao);
    sprintf(&texto2Player1[7], "%d", nave1.vidas);
    sprintf(&texto2Player2[7], "%d", nave2.vidas);

    glColor3f (1.0, 1.0, 1.0);
    escreveTexto(GLUT_BITMAP_HELVETICA_18, textoPlayer1, -80, -80, 0);
    escreveTexto(GLUT_BITMAP_HELVETICA_18, texto2Player1, -80, -90, 0);

    if (numPlayer == 2){
      escreveTexto(GLUT_BITMAP_HELVETICA_18, textoPlayer2, 20, -80, 0);
      escreveTexto(GLUT_BITMAP_HELVETICA_18, texto2Player2, 20, -90, 0);
    }

    glutSwapBuffers(); // Usa-se isso ao invés de glFlush, para trocar as telas
  }

}

void desenhaCenaInicial(){
  // T E X T U R A
  glColor3f (1, 1, 1);
  // Habilita o uso de texturas		    // Habilita o uso de texturas
  glEnable(GL_TEXTURE_2D);
  // Começa a usar a textura que criamos		    // Começa a usar a textura que criamos
  glBindTexture(GL_TEXTURE_2D, texturas[MAPA]);
   glBegin(GL_TRIANGLE_FAN);
      // Associamos um canto da textura para cada vértice		        // Associamos um canto da textura para cada vértice
      glTexCoord2f(0, 0); glVertex3f(-100, -100,  0);
      glTexCoord2f(1, 0); glVertex3f( 100, -100,  0);
      glTexCoord2f(1, 1); glVertex3f( 100,  100,  0);
      glTexCoord2f(0, 1); glVertex3f(-100,  100,  0);
  glEnd();
  glDisable(GL_TEXTURE_2D);

  // Escreve as coisas na telas
  glColor3f (0, 0, 0);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, "COKEoroides", -23, 40, 0);

  escreveTexto(GLUT_BITMAP_HELVETICA_18, "Escolha o modo que você deseja jogar:", -60, 0, 0);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, "1 Player | Player 1 vs Player 2 | Campanha", -70, -20, 0);

  glutSwapBuffers(); // Usa-se isso ao invés de glFlush, para trocar as telas

}

// REDIMENSIONA
void redimensiona(int w, int h){
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-100, 100, -100, 100, -1, 1); // Define a tela
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

// TECLADO
void teclado(unsigned char key, int x, int y){
  switch (key) {
    case 27:
      exit(0);
      break;

    case ' ':
    	tecladoState[SPACE] = 1;
    	break;

    // Para frente player 2
    case 'w':
    case 'W':
      tecladoState[W] = 1;
      break;

    // Para direita player 2
    case 'd':
    case 'D':
      tecladoState[D] = 1;
      break;

    // Para esquerda player 2
    case 'a':
    case 'A':
      tecladoState[A] = 1;
      break;

    // TIRO player 2
    case 'C':
    case 'c':
      tecladoState[C] = 1;
      break;

    // Para pausar o jogo
    case 'P':
    case 'p':
      if(tecladoState[P]){
        tecladoState[P] = 0;
      }
      else{
        tecladoState[P] = 1;
      }

    // Para reiniciar o jogo
    case 'R':
    case 'r':
      tecladoState[R] = 1;

    default:
      break;
  }
}

// TECLADO
void tecladoUP(unsigned char key, int x, int y){
  switch (key) {
    case ' ':
    	tecladoState[SPACE] = 0;
    	break;

    // Para frente player 2
    case 'w':
    case 'W':
      tecladoState[W] = 0;
      break;

    // Para direita player 2
    case 'd':
    case 'D':
      tecladoState[D] = 0;
      break;

    // Para esquerda player 2
    case 'a':
    case 'A':
      tecladoState[A] = 0;
      break;

    // TIRO player 2
    case 'C':
    case 'c':
      tecladoState[C] = 0;
      break;

    // Para reiniciar o jogo
    case 'R':
    case 'r':
      tecladoState[R] = 0;

    default:
      break;
  }
}

// Função para utilizar as setinhas
void teclado_Special(int key, int x, int y){

  switch (key) {

    case GLUT_KEY_LEFT:  // Seta para esquerda
      tecladoState[LEFT] = 1;
      break;

    case GLUT_KEY_RIGHT:  // Seta para direita
      tecladoState[RIGHT] = 1;
      break;

    case GLUT_KEY_UP:
      tecladoState[UP] = 1;
      break;

    default:
      break;
  }
}

// Função para utilizar as setinhas
void teclado_SpecialUP(int key, int x, int y){
  switch (key) {
    case GLUT_KEY_UP:
      tecladoState[UP] = 0;
      break;

    case GLUT_KEY_RIGHT:
      tecladoState[RIGHT] = 0;
      break;

    case GLUT_KEY_LEFT:
      tecladoState[LEFT] = 0;
      break;

    default:
      break;
  }
}

// ATUALIZA
void atualiza (int idx){
  // Caso o jogo não esteja pausado, atualiza o jogo
  if(!tecladoState[P]){
    if(stage == 3){
      //checaColisaoAsteroids();
      alteraTranslacaoNave(&nave1, &tecladoState);
      alteraTranslacaoNave(&nave2, &tecladoState);
      alteraTranslacaoAsteroid();
      alteraTranslacaoTiros();
      verificaDurabilidadeTiros();

      if(numPlayer == 2 && PvP == 1){
        checaColisaoNaveTiro(&nave2);
        checaColisaoNaveTiro(&nave1);
      }
      if(numPlayer == 2){
        checaColisaoTiroAsteroid(&nave2);
        checaColisaoNaveAsteroid(&nave2);
      }
      checaColisaoTiroAsteroid(&nave1);
      checaColisaoNaveAsteroid(&nave1);
    }
  }

  // Reinicia o jogo
  if(tecladoState[R])
    inicializa();

  glutPostRedisplay();
  glutTimerFunc(17, atualiza, 0);

}

void click(int button, int state, int x, int y){
  // Converte pixels para unidade de medida do jogo
  mouse.x = ((float)x)/glutGet(GLUT_WINDOW_WIDTH)*200 - 100;
  mouse.y = -((float)y)/glutGet(GLUT_WINDOW_HEIGHT)* 200 + 100;

  //  printf("\n\nX: %f", mouse.x);
  //  printf("\nY: %f", mouse.y);

  if(state == GLUT_DOWN && button == GLUT_LEFT_BUTTON){

    if(mouse.y > -22 && mouse.y < -15){
      // printf("\n\nX: %f", mouse.x);
      // printf("\nY: %f", mouse.y);

      // MODO DE JOGO CAMPANHA
      if (mouse.x > 30 && mouse.x < 70){
        nave1.vidas = nave2.vidas = 5;
        PvP = 0;
        // printf("\npassou");
        numPlayer = 2;
        stage = 3;
      }

      // MODO DE JOGO Player vs Player
      if(mouse.x > -40 && mouse.x < 30){
        nave1.vidas = nave2.vidas = 5;
        PvP = 1;
        numPlayer = 2;
        stage = 3;
      }

      // MODO DE JOGO SINGLE player
      if(mouse.x > -70 && mouse.x < -40){
        nave1.vidas = 5;
        numPlayer = 1;
        stage = 3;
      }
    }


      // stage = 3;
      // else if (x <= -23 && x >= 23){
    // }
  }
}
/*
void atualizaMapa(int idx){

  glutPostRedisplay();
  glutTimerFunc(80, atualizaMapa,0);
}*/
