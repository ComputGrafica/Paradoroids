#include "tiro.h"
#include "utilidades.h"
#include <stdio.h>

typedef enum textura{
  NAVE1 = 0,
  NAVE2,
  TIROTEX,
  ASTEROID1,
  ASTEROID2,
  ASTEROID3,
  ASTEROID4,
  ASTEROID5,
  ASTEROID6,
  MAPA
} TEXTURAS;


void iniciaListaTiros()
{
  listaInicioTiro = NULL;
  tiroAtual = NULL;
  numTiros = 0;
}

void criaTiro(GLfloat angulo, GLfloat posicaoX, GLfloat posicaoY, void *ponteiroParaNave)
{
  // Define o tiro atual para o início da lista
  tiroAtual = listaInicioTiro;
  // Caso haja nenhum tiro, cria um no início da lista de tiros
  if(listaInicioTiro == NULL){
    listaInicioTiro = (TIRO *)malloc(sizeof(TIRO));

    // O tiro atual aponta para o início da lista de tiros
    tiroAtual = listaInicioTiro;
  }
  else {
    // Viaja até o último tiro da lista
    for(; (*tiroAtual).proximo != NULL; tiroAtual = (*tiroAtual).proximo){}
    // Cria um próximo tiro;
    (*tiroAtual).proximo = (TIRO *)malloc(sizeof(TIRO));
    // Define o anterior do próximo tiro como tiro atual
    (*(*tiroAtual).proximo).anterior = tiroAtual;
    // Altera o tiro atual para o tiro criado, afim de facilitar a inserção de dados
    tiroAtual = (*tiroAtual).proximo;
  }
  // Inserindo os dados do tiro
  (*tiroAtual).proximo = NULL;
  (*tiroAtual).velocidadeTangencial = VELOCIDADE_TIRO;
  (*tiroAtual).anguloInicial = grausParaRadianos(angulo);
  (*tiroAtual).xTranslacao = posicaoX;
  (*tiroAtual).yTranslacao = posicaoY;
  (*tiroAtual).raio = 1;
  (*tiroAtual).distanciaPercorrida = 0;
  (*tiroAtual).ponteiroParaNave = ponteiroParaNave;
  numTiros++;
}

void alteraTranslacaoTiros()
{
  // Caso não existam tiros, ele ignora o procedimento
  if(listaInicioTiro == NULL)
    return ;

  // Define o tiro atual para o início da lista
  tiroAtual = listaInicioTiro;

  // Altera as translações dos tiros enquanto a lista não acabar
  for(; tiroAtual != NULL; tiroAtual = (*tiroAtual).proximo){
    // Para caso o tiro saia da tela, transporta o tiro para o outro lado
    detectaAlemDaTela(tiroAtual, 3);

    // Movimenta o tiro
    (*tiroAtual).xTranslacao += cos((*tiroAtual).anguloInicial) * (*tiroAtual).velocidadeTangencial;
    (*tiroAtual).yTranslacao += sin((*tiroAtual).anguloInicial) * (*tiroAtual).velocidadeTangencial;

    // Marca a distância percorrida pelo asteróide para cada tique
    (*tiroAtual).distanciaPercorrida += 4;
  }
}

void transladaTiro()
{
  // Caso não existam tiros, ele ignora o método
  if(listaInicioTiro == NULL)
    return ;

  // Define o tiro atual para o início da tela
  tiroAtual = listaInicioTiro;
  // Movendo o sistema de coordenadas para onde o tiro deve ser desenhado
  // e desenhando o tiro
  for(; tiroAtual != NULL; tiroAtual = (*tiroAtual).proximo){
    // Iniciando a pilha
    glPushMatrix();
    // Translada o tiro atual para uma sua posição
    glTranslatef((*tiroAtual).xTranslacao, (*tiroAtual).yTranslacao, 0);

    // glClear(GL_COLOR_BUFFER_BIT);
        glColor3f (1, 1, 1);
        // Habilita o uso de texturas
        glEnable(GL_TEXTURE_2D);
        // Começa a usar a textura que criamos
        glBindTexture(GL_TEXTURE_2D, texturas[TIROTEX]);
        glBegin(GL_TRIANGLE_FAN);
            // Associamos um canto da textura para cada vértice
            glTexCoord2f(0, 0); glVertex3f(-2, -2,  0);
            glTexCoord2f(1, 0); glVertex3f( 2, -2,  0);
            glTexCoord2f(1, 1); glVertex3f( 2,  2,  0);
            glTexCoord2f(0, 1); glVertex3f(-2,  2,  0);
        glEnd();
        glDisable(GL_TEXTURE_2D);

    // Desenhando o tiro
    desenhaCirculo((*tiroAtual).raio);
    // Descartando a pilha
    glPopMatrix();
  }
}

void verificaDurabilidadeTiros()
{
  // Caso não existam tiros, ele ignora o método
  if(listaInicioTiro == NULL)
    return ;

  // Define o tiro atual para o início da lista
  tiroAtual = listaInicioTiro;

  for(GLint numTiro = 0; tiroAtual != NULL; tiroAtual = (*tiroAtual).proximo, numTiro++) {
    if((*tiroAtual).distanciaPercorrida >= 128){  //128 800
      // Retira o tiro que já alcançou ou excedeu a distância
      retiraTiro(numTiro);
      // Retira um do número de tiros contados
      numTiro--;
    }

  }
}

void retiraTiro(GLint numTiro)
{

  TIRO *guardaTiro = NULL;

  // Caso o tiro seja o primeiro da lista
  if (numTiro == 0) {
    guardaTiro = listaInicioTiro;
    listaInicioTiro = (*listaInicioTiro).proximo;
    free(guardaTiro);
    return ;
  }

  TIRO *procuraTiro = listaInicioTiro;

  // Navega na lista até o tiro informado
  for(GLint contaTiros = 0; contaTiros != numTiro; procuraTiro = (*procuraTiro).proximo, contaTiros++){}

  // Guarda o elemento da lista para futura liberação de memória
  guardaTiro = procuraTiro;

  // Retira o tiro informado da lista
  (*(*procuraTiro).anterior).proximo = (*procuraTiro).proximo;
  (*(*procuraTiro).proximo).anterior = (*procuraTiro).proximo;

  // Libera a memória que estava reservada para o tiro informado
  free(guardaTiro);
  }
