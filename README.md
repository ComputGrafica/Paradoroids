# COKEoroides

Jogo desenvolvido como trabalho para a disciplina de Computação Gráfica, ministrada pelo Profº Flávio Coutinho, do curso de Engenharia da Computação do Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG).

O desenvolvimento desse jogo visava o aprendizado da biblioteca gráfica OpenGL, programação estruturada, boas práticas de programação e agregação de nota para o semestre na disciplina.

### Pré-requisitos para compilação do código-fonte

Para compilação do código fonte é necessária a instalação prévia das bibliotecas freeglut e glew, bem como um compilador de código-fonte em C, como o GCC.

### Executando

Para compilação e execução do código, basta conceder permissão de execução ao arquivo
```
compiler.sh
```

via terminal, com o comando

```
chmod +x compiler.sh
```

e executá-lo, também via terminal, com o comando

```
./compiler.sh
```
## Itens extras
* Prójétil orientado;
* Texturas diferentes para asteróides de tipos diferentes;
* Algumas telas;
* Modos de jogo:
    * Um player;
    * Player vs Player;
    * Modo cooperativo (Campanha)


## Feito com

* [freeglut](http://freeglut.sourceforge.net/) - Parte da biblioteca do OpenGL
* [glew](http://glew.sourceforge.net/) - Parte da biblioteca do OpenGL
* [Atom](https://atom.io/) - Editor de texto


## Autores

* **Arthur Novaes** -  [ArthurN](https://gitlab.com/ArthurN)
* **Marcelo Ferreira Cândido** - [marsCanHaveLife](https://gitlab.com/marsCanHaveLife)
