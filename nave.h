#include <SOIL/SOIL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "utilidades.h"
#include "tiro.h"

/*
* Declarações de variáveis, constantes, estruturas
* e métodos para o objeto nave
*/

// ### Defines ####
#define radianoParaGraus(radianos) (radianos * (180.0 / M_PI))
#define grausParaRadianos(graus) ((graus * M_PI) / 180.0)
#define PI 3.14159265359
#define VEL_MAX 2

// Estrutura para definição do objeto nave
typedef struct nave {
  // Posição da nave
  GLfloat xTranslacao, yTranslacao;
  // Velocidade tangencial da nave
  GLfloat velocidadeTangencial;
  GLfloat velocidadeTangencialX;
  GLfloat velocidadeTangencialY;
  // Variáveis para o ângulo da nave
  GLfloat anguloDeRotacaoEmRadianos; // Em radianos
  GLfloat anguloDeRotacaoEmGraus; // Em graus
  GLfloat velocidadeAngular;
  // Imagem da nave
  GLint imagem;
  // Tamanho da nave
  GLint raio;
  // Pontuação do jogador da respectiva nave
  GLint pontuacao;
  GLint vidas;
} NAVE;

//Inicializa as variáveis da nave
void inicializaVariaveisNave(NAVE *nave, GLfloat xTran, GLfloat yTran, GLfloat velTg);

// Comportamentos da nave

// Função para atualização das variáveis de posição
// da nave
void alteraTranslacaoNave();

// Função para atualização das variáveis de rotação
// da nave
void alteraRotacao(NAVE *nave, char sinal);

// Função para translação e rotação gráficas da nave
void transladaNaveComRotacao(NAVE *nave);

// Função para disparo da nave
void naveDispara();

// Função para explosão da nave
void naveExplode();

// Função que vai criar a inercia da nave
void naveAcelera(NAVE *nave, char sinal);
