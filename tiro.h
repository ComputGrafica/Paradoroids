#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>
#include <stdlib.h>
#include "utilidades.h"
//#include "nave.h"

/*
* Declarações de variáveis, constantes, estruturas
* e métodos para a classe tiro
*/

// ### Defines ###
#define PI 3.14159265359
#define radianoParaGraus(radianos) (radianos * (180.0 / M_PI))
#define grausParaRadianos(graus) ((graus * M_PI) / 180.0)
#define VELOCIDADE_TIRO 4

// Struct que define as propriedades do tiro
typedef struct tiros {
  struct tiros *anterior;
  GLfloat anguloInicial;
  GLfloat xTranslacao;
  GLfloat yTranslacao;
  GLfloat velocidadeTangencial;
  GLfloat raio;
  GLfloat distanciaPercorrida;
  void *ponteiroParaNave;
  struct tiros *proximo;
} TIRO;

// Lista de tiros da nave
TIRO *listaInicioTiro;
TIRO *tiroAtual;

// Variáveis globais
int numTiros;

// Funções e procedimentos relacionadas à classe tiro

// Inicia a lista de tiros
void iniciaListaTiros();

// Procedimento para adicionar um tiro à lista
void criaTiro(GLfloat angulo, GLfloat posicaoX, GLfloat posicaoY, void *nave);

// Altera as variáveis de translação dos tiros
void alteraTranslacaoTiros();

// Procedimento para movimentação do tiro
void transladaTiro();

// Procedimento que verifica se um tiro já deveria não mais existir
void verificaDurabilidadeTiros();

// Quando um tiro alcança sua durabilidade, esse procedimento o tira da lista
void retiraTiro(GLint numTiro);
