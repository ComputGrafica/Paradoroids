#include "nave.h"
#include "utilidades.h"

#define UP  0
#define W   5

typedef enum textura{
  NAVE1 = 0,
  NAVE2,
  TIROTEX,
  ASTEROID1,
  ASTEROID2,
  ASTEROID3,
  ASTEROID4,
  ASTEROID5,
  ASTEROID6,
  MAPA
} TEXTURAS;

// Inicialização das variáveis da nave
/***************************************************
 * Função: inicializaVariaveisNave()
 * Entrada: Nenhuma (void)
 * Saída:	Nenhuma (void)
 * Descrição: inicializa as varíaveis da nave
 **************************************************/
void inicializaVariaveisNave(NAVE *nave, GLfloat xTran, GLfloat yTran, GLfloat velTg){

  /*************************** N A V E  U M ***********************************/
  // Posição inicial
  (*nave).xTranslacao = xTran;
  (*nave).yTranslacao = yTran;
  // Tamanho inicial
  (*nave).raio = 6.0;
  // Ângulo inicial da nave
  (*nave).anguloDeRotacaoEmGraus = 0.0;
  (*nave).anguloDeRotacaoEmRadianos = 0.0;
  // Velocidades tangencial e angular iniciais
  (*nave).velocidadeTangencial = velTg;
  (*nave).velocidadeAngular = 0.05;
  (*nave).pontuacao = 0;
}

// Definindo os métodos de comportamento da nave

/***************************************************
 * Função: alteraTranslacaoNave()
 * Entrada: Nenhuma (void)
 * Saída:	Nenhuma (void)
 * Descrição: altera as variáveis de posição da nave
 **************************************************/
void alteraTranslacaoNave(NAVE *nave, int *tecladoState)
{
    // A nave so pode mudar de direção quando a tecla para frente for precionada
    if (tecladoState[UP] || tecladoState[W]){
      (*nave).anguloDeRotacaoEmRadianos = grausParaRadianos((*nave).anguloDeRotacaoEmGraus);
    }

    // Verifica se a nave está fora da tela
    detectaAlemDaTela(nave, 2);

    (*nave).xTranslacao += (*nave).velocidadeTangencialX;
    (*nave).yTranslacao += (*nave).velocidadeTangencialY;
}

/***************************************************
 * Função:	void alteraRotacao(char sinal)
 * Entrada:	sinal: incremento ou decremento para a velocidade angular da nave
 * Saída:	Nenhuma (void)
 * Descrição: Incrementa ou decrementa a velocidade angular da nave
 **************************************************/
void alteraRotacao(NAVE *nave, char sinal)
{
  switch (sinal) {
    case '+':
    (*nave).velocidadeAngular = 6;
    // (*nave).anguloDeRotacaoEmGraus = 180;
      break;
    case '-':
    (*nave).velocidadeAngular = -6;
      break;
    default:
      break;
  }

  (*nave).anguloDeRotacaoEmGraus += (*nave).velocidadeAngular;

  // printf("\nAngulo = %f", (*nave).anguloDeRotacaoEmRadianos);
}

/***************************************************
 * Função: void transladaNaveComRotacao()
 * Entrada: Nenhuma (void)
 * Saída:	Nenhuma (void)
 * Descrição: movimenta a nave já com a rotação
 **************************************************/
void transladaNaveComRotacao(NAVE *nave)
{
  glColor3f(0.5, 0.5, 1);

  // Iniciando a pilha
  glPushMatrix();
    // Movendo o sistema de coordenadas para onde a nave deve ser desenhada
    glTranslatef((*nave).xTranslacao, (*nave).yTranslacao, 0);

    // Rotacionando o sistema de coordenadas no eixo Z onde o cursor está localizado agora
    // O ângulo é passado em graus
    glRotatef((*nave).anguloDeRotacaoEmGraus, 0, 0, 1);


    // T E X T U R A
    glColor4f (1, 1, 1, -1000);
    // Habilita o uso de texturas		    // Habilita o uso de texturas
    glEnable(GL_TEXTURE_2D);
    // Começa a usar a textura que criamos		    // Começa a usar a textura que criamos
    glBindTexture(GL_TEXTURE_2D, texturas[(*nave).imagem]);
     glBegin(GL_TRIANGLE_FAN);
        // Associamos um canto da textura para cada vértice

        glTexCoord2f(0, 0); glVertex3f(-6, -4,  0);
        glTexCoord2f(1, 0); glVertex3f( 6, -4,  0);
        glTexCoord2f(1, 1); glVertex3f( 6,  4,  0);
        glTexCoord2f(0, 1); glVertex3f(-6,  4,  0);

    glEnd();
    glDisable(GL_TEXTURE_2D);
    // desenhaCirculo((*nave).raio);
    // Desenhando a nave



    glPopMatrix(); // Descartando a pilha
}

void naveAcelera(NAVE *nave, char sinal){
  GLfloat mudax, muday, modulo = 1;

  switch (sinal){
    case '+':

      mudax = cos((*nave).anguloDeRotacaoEmRadianos) * 0.08;  // Incremento da velocidad em X
      muday = sin((*nave).anguloDeRotacaoEmRadianos) * 0.08;  // Incremento da velocidad em Y
      // printf("\nAngulo = %f", (*nave).anguloDeRotacaoEmRadianos);

      (*nave).velocidadeTangencialX += mudax;
      (*nave).velocidadeTangencialY += muday;

      //printf("\nVelocidade B MOD: %f", nave->velocidadeTangencialX);



        modulo = sqrt(
                        pow (((*nave).velocidadeTangencialX), 2) +
                        pow (((*nave).velocidadeTangencialY), 2)
                      );
        if (modulo >= 1){
          if ((*nave).velocidadeTangencialX != 0)
            (*nave).velocidadeTangencialX = (*nave).velocidadeTangencialX / modulo;
          if ((*nave).velocidadeTangencialY != 0)
            (*nave).velocidadeTangencialY = (*nave).velocidadeTangencialY / modulo;
          }

        // printf("Velocidade em X %f\n", (*nave).velocidadeTangencialX);
        // printf("Velocidade em Y %f\n", (*nave).velocidadeTangencialY);


      break;

    // DESACELERA A NAVE
    case '-':
      //if ((*nave).velocidadeTangencial >= 0){
        mudax = cos((*nave).anguloDeRotacaoEmRadianos) * 0.01;
        muday = sin((*nave).anguloDeRotacaoEmRadianos) * 0.01;

        // em X
        if (((*nave).velocidadeTangencialX > 0 && mudax > 0) || ((*nave).velocidadeTangencialX < 0 && mudax < 0)){
          (*nave).velocidadeTangencialX -= mudax;
        }

        /*else if ((*nave).velocidadeTangencialX < 0 && mudax < 0){
          (*nave).velocidadeTangencialX -= mudax;
        }*/
        else{
          (*nave).velocidadeTangencialX = 0;
        }

        // em Y
        if (((*nave).velocidadeTangencialY > 0 && muday > 0)  || ((*nave).velocidadeTangencialY < 0 && muday < 0)){
          (*nave).velocidadeTangencialY -= muday;
        }
/*
        else if (){
          (*nave).velocidadeTangencialY -= muday;
        }*/
        else{
          (*nave).velocidadeTangencialY = 0;
        }

        // modulo = sqrt(
        //                 pow (((*nave).velocidadeTangencialX), 2) +
        //                 pow (((*nave).velocidadeTangencialY), 2)
        //               );
        // if ((*nave).velocidadeTangencialX != 0)
        //   (*nave).velocidadeTangencialX = (*nave).velocidadeTangencialX / modulo;
        // if ((*nave).velocidadeTangencialY != 0)
        //   (*nave).velocidadeTangencialY = (*nave).velocidadeTangencialY / modulo;

          //printf("Velocidade em X %f\n", (*nave).velocidadeTangencialX);


      break;
    default:
      break;
  }

  // printf("\nAceleração em X %f\n", mudax);
  // printf("Velocidade em X %f\n", (*nave).velocidadeTangencialX);


}

void naveDispara(NAVE *nave)
{
  criaTiro((*nave).anguloDeRotacaoEmGraus, (*nave).xTranslacao, (*nave).yTranslacao, nave);
}

void naveExplode()
{

}
