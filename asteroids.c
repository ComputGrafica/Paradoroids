#include <stdio.h>
#include "asteroids.h"


typedef enum textura{
  NAVE1 = 0,
  NAVE2,
  TIROTEX,
  ASTEROID1,
  ASTEROID2,
  ASTEROID3,
  ASTEROID4,
  ASTEROID5,
  ASTEROID6,
  MAPA
} TEXTURAS;


GLfloat geraPosicoesAleatorias()
{
  // Determina o lado que a posição será gerada
  // 0 para embaixo ou à esquerda
  // 1 para em cima ou à direita
  GLint lado = rand() % 2;

  if(lado)
    return (100.0 - rand() % 50);
  else
    return (-100.0 + rand() % 50);

}

/***************************************************
 * Procedimento: inicializaListaAsteroids()
 * Entrada: Nenhuma (void)
 * Saída:	Nenhuma (void)
 * Descrição: inicializa a lista de asteróides
 **************************************************/
void inicializaListaAsteroids()
{
  // Garantindo que a lista de asteróides será iniciada (ou reiniciada) do zero
  listaInicioAsteroid = NULL;
  // Para garantir números aleatórios
  srand(time(0));
  // Define um número randômico de asteróides
  int numAsteroids = 5 + rand() % 5;

  // Cria um novo asteróide
  listaInicioAsteroid = (ASTEROID *)malloc(sizeof(ASTEROID));
  (*listaInicioAsteroid).anterior = NULL;
  asteroidAtual = listaInicioAsteroid;

    if((rand() % 2) == 1) (*listaInicioAsteroid).imagem = ASTEROID4;
  else (*listaInicioAsteroid).imagem = ASTEROID1;

  // Continua a lista de asteróides
  for(int contador = 1; contador < numAsteroids; contador++){

    (*asteroidAtual).anguloDeRotacaoEmGraus = rand() % 360;

    // Preenche as variáveis de cada asteróide
    (*asteroidAtual).xTranslacao = geraPosicoesAleatorias();
    (*asteroidAtual).yTranslacao = geraPosicoesAleatorias();
    (*asteroidAtual).velocidadeTangencial = (GLfloat) 0.2 + rand() % 1;
    (*asteroidAtual).anguloInicialEmGraus = (GLfloat) (rand() % 360);
    (*asteroidAtual).anguloInicialEmRadianos = grausParaRadianos((*asteroidAtual).anguloInicialEmGraus);
    (*asteroidAtual).raio = 7 + rand() % 7;
    // Cria o próximo asteróide
    (*asteroidAtual).proximo = (ASTEROID *)malloc(sizeof(ASTEROID));

    // Define o asteróide anterior do próximo asteróide (que é o asteróide que você está no momento)
    (*(*asteroidAtual).proximo).anterior = asteroidAtual;

    // Pula para o próximo asteróide
    asteroidAtual = (*asteroidAtual).proximo;

    if((rand() % 2) == 1) (*asteroidAtual).imagem = ASTEROID4;
    else (*asteroidAtual).imagem = ASTEROID1;

  }
  // O último asteróide não possui próximo asteróide
  (*asteroidAtual).proximo = NULL;
}

/***************************************************
 * Procedimento: alteraTranslacaoAsteroid()
 * Entrada: Nenhuma (void)
 * Saída:	Nenhuma (void)
 * Descrição: altera a translação dos asteróides

 **************************************************/
void alteraTranslacaoAsteroid()
{
  // Altera translações dos asteróides a partir do ínicio da lista
  asteroidAtual = listaInicioAsteroid;

  // Altera as translações dos asteróides enquanto a lista não acabar
  for(; asteroidAtual != NULL; asteroidAtual = (*asteroidAtual).proximo){
    // Verifica se o asteróide atual está fora da tela
    detectaAlemDaTela(asteroidAtual, 1);

    (*asteroidAtual).yTranslacao += sin((*asteroidAtual).anguloInicialEmRadianos) * (*asteroidAtual).velocidadeTangencial;
    (*asteroidAtual).xTranslacao += cos((*asteroidAtual).anguloInicialEmRadianos) * (*asteroidAtual).velocidadeTangencial;
  }
}

/***************************************************
* Procedimento: transladaAsteroid()
* Entrada: Nenhuma (void)
* Saída:	Nenhuma (void)
* Descrição: desenha os asteróides em sua nova translação
**************************************************/
void transladaAsteroid()
{

  // Definindo a cor do asteróide como preta
  glColor3f(0, 0, 0);

  // Inicia translação a partir do inicio da lista de asteróides
  asteroidAtual = listaInicioAsteroid;

  // Movendo o sistema de coordenadas para onde o asteróide deve ser desenhado
  for(; asteroidAtual != NULL; asteroidAtual = (*asteroidAtual).proximo){
    //@TODO: Colocar imagem aleatória


      // Ini'ciando a pilha
      glPushMatrix();
      // Translada o asteróide atual para uma sua posição
      glTranslatef((*asteroidAtual).xTranslacao, (*asteroidAtual).yTranslacao, 0);
      // Desenhando o asteróide

      glRotatef((*asteroidAtual).anguloDeRotacaoEmGraus, 0, 0, 1);

      // T E X T U R A
      glColor4f (1, 1, 1, -1000);
      // Habilita o uso de texturas		    // Habilita o uso de texturas
      glEnable(GL_TEXTURE_2D);
      // Começa a usar a textura que criamos		    // Começa a usar a textura que criamos
      glBindTexture(GL_TEXTURE_2D, texturas[(*asteroidAtual).imagem]);
       glBegin(GL_TRIANGLE_FAN);
          // Associamos um canto da textura para cada vértice

          glTexCoord2f(0, 0); glVertex3f(-(*asteroidAtual).raio/2, -(*asteroidAtual).raio,  0);
          glTexCoord2f(1, 0); glVertex3f( (*asteroidAtual).raio/2, -(*asteroidAtual).raio,  0);
          glTexCoord2f(1, 1); glVertex3f( (*asteroidAtual).raio/2,  (*asteroidAtual).raio,  0);
          glTexCoord2f(0, 1); glVertex3f(-(*asteroidAtual).raio/2,  (*asteroidAtual).raio,  0);

      glEnd();
      glDisable(GL_TEXTURE_2D);

      // desenhaCirculo((*asteroidAtual).raio);
      // Descartando a pilha
      glPopMatrix();
  }

}

void criaNovosAsteroides(GLfloat posicaoX, GLfloat posicaoY)
{
  // Gera de 2 a 3 novos asteróides
  GLint numNovosAsteroides = 2 + rand() % 2;

  // Busca o último asteróide da lista a partir do primeiro
  asteroidAtual = listaInicioAsteroid;

  for(; (*asteroidAtual).proximo != NULL; asteroidAtual = (*asteroidAtual).proximo){}

  // Cria novos asteroides a partir da posição do asteróide partido, que foi passada por parâmetro
  for(GLint contaAsteroids = 0; contaAsteroids < numNovosAsteroides; contaAsteroids++){
    // Criando novo asteróide no fim da lista
    (*asteroidAtual).proximo = (ASTEROID *)malloc(sizeof(ASTEROID));
    // Definindo o anterior do próximo asteróide como o asteróide atual
    (*(*asteroidAtual).proximo).anterior = asteroidAtual;
    // Definindo o asteróide atual como o novo criado
    asteroidAtual = (*asteroidAtual).proximo;
    // Definindo a posição do novo asteróide como a do asteróide partido
    (*asteroidAtual).xTranslacao = posicaoX;
    (*asteroidAtual).yTranslacao = posicaoY;
    // Definindo as outras propriedades do asteróide
    // Velocidade maior que a original por conta do impacto do tiro
    (*asteroidAtual).velocidadeTangencial = (GLfloat) 0.6 + rand() % 2;
    (*asteroidAtual).anguloInicialEmGraus = (GLfloat) (rand() % 360);
    (*asteroidAtual).anguloInicialEmRadianos = grausParaRadianos((*asteroidAtual).anguloInicialEmGraus);
    (*asteroidAtual).raio = 3 + rand() % 3;

    if((rand() % 2) == 1) (*asteroidAtual).imagem = ASTEROID5;
    else (*asteroidAtual).imagem = ASTEROID3;

    (*asteroidAtual).anguloDeRotacaoEmGraus = rand() % 360;
  }
  (*asteroidAtual).proximo = NULL;
}

void colapsaAsteroid(ASTEROID *asteroidAtingido)
{
  // Procura o asteróide a partir do início da lista
  ASTEROID *guardaAsteroid = NULL;

  // Variáveis para salvar a posição do asteróide atingido
  GLfloat salvaPosicaoX;
  GLfloat salvaPosicaoY;
  GLfloat salvaRaio;

  // Salva os dados do asteróide
  salvaPosicaoX = (*asteroidAtingido).xTranslacao;
  salvaPosicaoY = (*asteroidAtingido).yTranslacao;
  salvaRaio = (*asteroidAtingido).raio;

  // Guarda o endereço do asteróide para posterior liberação de memória
  guardaAsteroid = asteroidAtingido;

  // Caso o tiro seja o primeiro da lista
  if (asteroidAtingido == listaInicioAsteroid) {

    // Elimina o primeiro da lista
    asteroidAtingido = (*asteroidAtingido).proximo;
    (*asteroidAtingido).anterior = NULL;
    listaInicioAsteroid = asteroidAtingido;

  } else {
    // Elimina o asteróide atingido ligando o próximo do asteróide anterior com o anterior do próximo asteróide
    (*(*asteroidAtingido).anterior).proximo = (*asteroidAtingido).proximo;
    (*(*asteroidAtingido).proximo).anterior = (*asteroidAtingido).anterior;

  }

  // Elimina a memória do asteróide que foi atingido
  free(guardaAsteroid);

  // Caso o raio do asteróide atingido fosse maior que 6, cria novos asteróides
  if (salvaRaio > 6)
    criaNovosAsteroides(salvaPosicaoX, salvaPosicaoY);
}
