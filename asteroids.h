#include <GL/glew.h>
#include <GL/freeglut.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "utilidades.h"

/*
* Declarações de variáveis, constantes, estruturas
* e métodos para a classe asteróide
*/

// ### Defines ###
#define PI 3.14159265359
#define radianoParaGraus(radianos) (radianos * (180.0 / M_PI))
#define grausParaRadianos(graus) ((graus * M_PI) / 180.0)

// Estrutura para a definição da classe asteróide
typedef struct asteroid {
  // Ponteiro para o asteróide anterior
  struct asteroid *anterior;
  // Posição do asteróide
  GLfloat xTranslacao, yTranslacao;
  // Velocidade tangencial do asteróide
  GLfloat velocidadeTangencial;
  // Angulo (em graus) de direção inicial do asteróide
  GLfloat anguloInicialEmGraus;
  // Angulo (em radianos) de direção inicial do asteróide
  GLfloat anguloInicialEmRadianos;

  GLfloat anguloDeRotacaoEmGraus; // Em graus
  // Tamanho do asteróide
  GLint raio;
  // Textura do asteroid
  GLint imagem;
  // Ponteiro para o próximo asteróide
  struct asteroid *proximo;
} ASTEROID;

// Lista de asteróides
ASTEROID *listaInicioAsteroid;
ASTEROID *asteroidAtual;

// Funções relacionados à classe asteróide

// Cria posições aleatórias para os asteróides a fim de que não matem a nave no início do jogo
GLfloat geraPosicoesAleatorias();

// Procedimento para inicialização das varíaveis do asteróide
void inicializaListaAsteroids();

// Procedimento para atualização das variáveis
void alteraTranslacaoAsteroid();

// Devido a colisão de um tiro com um asteróide, cria novos asteróides
void criaNovosAsteroides(GLfloat posicaoX, GLfloat posicaoY);

// Explode o asteróide em pedaços (ou o elimina, dependendo do seu raio)
void colapsaAsteroid(ASTEROID *asteroidAtingido);

// Procedimento para movimentação do asteróide, conservando seu angulo inicial
void transladaAsteroid();
