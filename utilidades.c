#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>
#include <stdio.h>
#include "utilidades.h"
#include "nave.h"
#include "asteroids.h"

typedef enum textura{
  NAVE1 = 0,
  NAVE2,
  TIROTEX,
  ASTEROID1,
  ASTEROID2,
  ASTEROID3,
  ASTEROID4,
  ASTEROID5,
  ASTEROID6,
  MAPA
} TEXTURAS;

void desenhaCirculo(GLint raioDoCoiso)
{
  // Determina o modo de desenho do OpenGL
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  // Cria o "círculo" com um número VERTICES de lados
  glBegin(GL_LINE_LOOP);
    for(int contador = 0; contador < VERTICES; contador++){
      GLfloat angulo = 2 * M_PI * contador / VERTICES;
      GLfloat coordenadaX = raioDoCoiso * cos(angulo);
      GLfloat coordenadaY = raioDoCoiso * sin(angulo);
      glVertex2f(coordenadaX, coordenadaY);
    }
  glEnd();
}

void detectaAlemDaTela(void *ponteiroGenerico, GLint tipo)
{

  // Tipo 01 para a nave
  if (tipo == 1) {
    ASTEROID *coiso = (ASTEROID *) ponteiroGenerico;

    // Se o objeto está completamente fora da tela em relação ao eixo x, inverte a posição x
    // do objeto
    if ((((*coiso).xTranslacao > 100 + (*coiso).raio)) || ((*coiso).xTranslacao < -100 - (*coiso).raio))
      (*coiso).xTranslacao *= -1;

    // Se o objeto está completamente fora da tela em relação ao eixo y, inverte a posição y
    // do objeto
    if ((((*coiso).yTranslacao > 100 + (*coiso).raio)) || ((*coiso).yTranslacao < -100 - (*coiso).raio))
      (*coiso).yTranslacao *= -1;


    // Tipo 02 para a asteróide
  } else if (tipo == 2) {
    NAVE *coiso = (NAVE *) ponteiroGenerico;

    // Se o objeto está completamente fora da tela em relação ao eixo x, inverte a posição x
    // do objeto
    if ((((*coiso).xTranslacao > 100 + 2 * (*coiso).raio)) || ((*coiso).xTranslacao < -100 - 2 * (*coiso).raio))
      (*coiso).xTranslacao *= -1;

    // Se o objeto está completamente fora da tela em relação ao eixo y, inverte a posição y
    // do objeto
    if ((((*coiso).yTranslacao > 100 + 2 * (*coiso).raio)) || ((*coiso).yTranslacao < -100 - 2 * (*coiso).raio))
      (*coiso).yTranslacao *= -1;

  } else if (tipo == 3){
    TIRO *coiso = (TIRO *) ponteiroGenerico;

    // Se o objeto está completamente fora da tela em relação ao eixo x, inverte a posição x
    // do objeto
    if ((((*coiso).xTranslacao > 100 + 2 * (*coiso).raio)) || ((*coiso).xTranslacao < -100 - 2 * (*coiso).raio))
      (*coiso).xTranslacao *= -1;

    // Se o objeto está completamente fora da tela em relação ao eixo y, inverte a posição y
    // do objeto
    if ((((*coiso).yTranslacao > 100 + 2 * (*coiso).raio)) || ((*coiso).yTranslacao < -100 - 2 * (*coiso).raio))
      (*coiso).yTranslacao *= -1;
  }
}

void checaColisaoTiroAsteroid()
{
  if(listaInicioTiro == NULL)
    return ;

  // Verifica a lista de tiros a partir do início
  tiroAtual = listaInicioTiro;
  // Verifica a lista de asteróides a partir do início
  asteroidAtual = listaInicioAsteroid;

  // Verifica se para cada tiro da lista houve colisão com cada asteróide da lista
  for(GLint numTiro = 0; tiroAtual != NULL; tiroAtual = (*tiroAtual).proximo, numTiro++, asteroidAtual = listaInicioAsteroid){
    for(GLint numAsteroid = 0; asteroidAtual != NULL; asteroidAtual = (*asteroidAtual).proximo, numAsteroid++){

      // Determinando as diferenças entre as coordenadas x e y do tiro com o asteróide
      GLfloat distX = (*tiroAtual).xTranslacao - (*asteroidAtual).xTranslacao;
      GLfloat distY = (*tiroAtual).yTranslacao - (*asteroidAtual).yTranslacao;

      // Determinado a soma dos raios do tiro com o asteróide
      GLfloat somaRaios = (*tiroAtual).raio + (*asteroidAtual).raio;

      // Compara se a distância entre os centros do tiro e do asteróide são iguais ou menores
      // que a soma de seus raios
      if (sqrt(pow(distX, 2) + pow(distY, 2)) <= somaRaios){
        printf("\n%d\n", numAsteroid);
        NAVE *nave = (NAVE *) (*tiroAtual).ponteiroParaNave;
        (*nave).pontuacao++;
        retiraTiro(numTiro);
        colapsaAsteroid(asteroidAtual);
      }
    }
  }

}

void checaColisaoNaveAsteroid(void *ponteiroParaNave)
{
  static int liberaColisao = 1;
  NAVE *nave = (NAVE *) ponteiroParaNave;

  if(listaInicioAsteroid == NULL)
    return ;

  asteroidAtual = listaInicioAsteroid;

  for (; asteroidAtual != NULL; asteroidAtual = (*asteroidAtual).proximo){

    GLfloat distX = (*nave).xTranslacao - (*asteroidAtual).xTranslacao;
    GLfloat distY = (*nave).yTranslacao - (*asteroidAtual).yTranslacao;
    GLfloat distMOD = sqrt(pow(distX, 2) + pow(distY, 2));
    GLfloat somaRaios = (*nave).raio + (*asteroidAtual).raio;

    if (distMOD <= somaRaios && liberaColisao == 1){
      // printf("\n\npassou");
      (*nave).vidas --;
      liberaColisao = 0;
    }
    //@TODO consertar isso aqui
    else if (distMOD < somaRaios+1 && distMOD > somaRaios && liberaColisao == 0){
      liberaColisao = 1;
      // printf("\n\nDistancia: %f", distMOD);
      // printf("\nLiberou, soma: %f", somaRaios);
    }

  }
}

void checaColisaoNaveTiro(NAVE *nave)
{
  static int liberaColisao = 1;
  GLfloat newX, newY, somRaio, distMOD;

  for (tiroAtual = listaInicioTiro; tiroAtual != NULL; tiroAtual = (*tiroAtual).proximo){
    newX = (*nave).xTranslacao - (*tiroAtual).xTranslacao;
    newY = (*nave).yTranslacao - (*tiroAtual).yTranslacao;
    somRaio = (*nave).raio + (*tiroAtual).raio;
    distMOD = sqrt((newX * newX) + (newY * newY)); // Distância modular

    if (distMOD <= somRaio && liberaColisao == 1){
      // printf("\n\npassou");
      (*nave).vidas --;
      liberaColisao = 0;
    }
    else if (distMOD < somRaio+1 && distMOD > somRaio && liberaColisao == 0){
      liberaColisao = 1;
      // printf("\n\nDistancia: %f", distMOD);
      // printf("\nLiberou, soma: %f", somaRaios);
    }
  }
}

void trocaVariaveisAsteroids(void *ponteiroParaAsteroid1, void *ponteiroParaAsteroid2)
{
  ASTEROID *asteroid1 = (ASTEROID *) ponteiroParaAsteroid1;
  ASTEROID *asteroid2 = (ASTEROID *) ponteiroParaAsteroid2;

  GLfloat auxFloat;

  auxFloat = (*asteroid1).velocidadeTangencial;
  (*asteroid1).velocidadeTangencial = (*asteroid2).velocidadeTangencial;
  (*asteroid2).velocidadeTangencial = auxFloat;

  auxFloat = (*asteroid1).anguloInicialEmRadianos;
  (*asteroid1).anguloInicialEmRadianos = (*asteroid2).anguloInicialEmRadianos;
  (*asteroid2).anguloInicialEmRadianos = auxFloat;
}

void checaColisaoAsteroids()
{
  if (listaInicioAsteroid == NULL)
    return ;

  ASTEROID *asteroidAtual1 = listaInicioAsteroid;
  ASTEROID *asteroidAtual2 = NULL;

  GLfloat distX, distY, somaRaios;

  for(; asteroidAtual1 != NULL; asteroidAtual1 = (*asteroidAtual1).proximo){
    for(asteroidAtual2 = listaInicioAsteroid; asteroidAtual2 != NULL; asteroidAtual2 = (*asteroidAtual2).proximo){

      if (asteroidAtual2 == asteroidAtual1)
        asteroidAtual2 = (*asteroidAtual2).proximo;

      distX = (*asteroidAtual1).xTranslacao - (*asteroidAtual2).xTranslacao;
      distY = (*asteroidAtual1).yTranslacao - (*asteroidAtual2).yTranslacao;
      somaRaios = (*asteroidAtual1).raio + (*asteroidAtual2).raio;

      if (sqrt(pow(distX, 2) + pow(distY, 2)) <= somaRaios)
        trocaVariaveisAsteroids(asteroidAtual1, asteroidAtual2);
    }
  }
}
// Configura a textura
void init(void)
{
  carregaTextura(&texturas[NAVE1],      "Ibagens/nave1.png");
  carregaTextura(&texturas[NAVE2],      "Ibagens/nave2.png");
  carregaTextura(&texturas[TIROTEX],    "Ibagens/tiro.jpg");
  carregaTextura(&texturas[MAPA],       "Ibagens/mapa.png");
  carregaTextura(&texturas[ASTEROID1],  "Ibagens/coke1.jpg");
  carregaTextura(&texturas[ASTEROID4],  "Ibagens/coke4.jpg");
  carregaTextura(&texturas[ASTEROID3],  "Ibagens/coke3.jpg");
  carregaTextura(&texturas[ASTEROID5],  "Ibagens/coke5.jpg");
}

void carregaTextura(GLuint *textura, char* nomeDoArquivo)
{
    *textura = SOIL_load_OGL_texture(
      nomeDoArquivo,
      SOIL_LOAD_AUTO,
      SOIL_CREATE_NEW_ID,
      SOIL_FLAG_INVERT_Y);
    if (*textura == 0) {
        printf("Erro ao carregar textura '%s': %s\n",
            nomeDoArquivo,
            SOIL_last_result());
    }
}

void desenhaFundo()
{
  // T E X T U R A
  glColor3f (0.1, 0.8, 0.1);
  // Habilita o uso de texturas		    // Habilita o uso de texturas
  glEnable(GL_TEXTURE_2D);
  // Começa a usar a textura que criamos		    // Começa a usar a textura que criamos
  glBindTexture(GL_TEXTURE_2D, texturas[MAPA]);
   glBegin(GL_TRIANGLE_FAN);
      // Associamos um canto da textura para cada vértice		        // Associamos um canto da textura para cada vértice
      glTexCoord2f(0, 0); glVertex3f(-100, -100,  0);
      glTexCoord2f(1, 0); glVertex3f( 100, -100,  0);
      glTexCoord2f(1, 1); glVertex3f( 100,  100,  0);
      glTexCoord2f(0, 1); glVertex3f(-100,  100,  0);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  // Desenhando a nave
}

void escreveTexto(void *fonte, char *s, float x, float y, float z)
{
  int i;

  glRasterPos3f(x, y, z);
  for(i = 0; i < strlen(s); i++){
    glutBitmapCharacter(fonte, s[i]);
  }
}
