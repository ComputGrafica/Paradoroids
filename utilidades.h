#include <SOIL/SOIL.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>
#include <string.h>
//#include "nave.h"

// ### Defines ###
#define VERTICES 100

// Para textura
GLuint texturas[10];

GLint pontuaP1;
GLint pontuaP2;

int PvP;  // Player vs Player


// Procedimento para desenhar um círculo na tela com um raio passado por parâmetro
void desenhaCirculo(GLint raioDoCoiso);

// Procedimento para detectar se um objeto saiu da tela, recolocando-o no outro lado
void detectaAlemDaTela(void *ponteiroGenerico, GLint tipo);

// Verifica a colisão entre os asteróides e os tiros
void checaColisaoTiroAsteroid();

// Verifica a colisão entre os asteróides e a nave
void checaColisaoNaveAsteroid(void *ponteiroParaNave);

// Troca as variáveis entre os asteróides em caso de colisão
void trocaVariaveisAsteroids(void *ponteiroParaAsteroid1, void *ponteiroParaAsteroid2);

// Checa a colisão entre asteróides, interpretando como uma colisão perfeitamente elástica
void checaColisaoAsteroids();

//Para textura
void init(void);

// Carrega texturas
void init(void);

void carregaTextura(GLuint *textura, char* nomeDoArquivo);

void desenhaFundo();

// Função para escrever a pontuação na tela
void escreveTexto(void * font, char *s, float x, float y, float z);
