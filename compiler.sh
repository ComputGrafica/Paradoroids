#!/bin/bash

# Compilando códigos-fonte
gcc -o utilidades.o -c utilidades.c -Wall
gcc -o nave.o -c nave.c -Wall
gcc -o main.o -c main.c -Wall
gcc -o asteroids.o -c asteroids.c -Wall
gcc -o tiro.o -c tiro.c -Wall

# Fazendo a linkedição
gcc -o jogo.exe tiro.o asteroids.o utilidades.o nave.o main.o -lglut -lGL -lGLU -lSOIL -lm

# Removendo arquivos *.o
rm *.o

# Executando o jogo
./jogo.exe &

# Removendo o jogo
rm jogo.exe
